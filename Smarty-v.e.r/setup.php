<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.10.16
 * Time: 23:50
 */
define('SMARTY_DIR', '/usr/share/nginx/html/everCalck/Smarty-v.e.r/');
require(SMARTY_DIR . 'Smarty.class.php');

class Smarty_everCalck extends Smarty {

    function Smarty_everCalcks()
    {
        $this->__construct();
        $this->Smarty_everCalcks();

        $this->template_dir = '/usr/share/nginx/html/everCalck/templates/';
        $this->compile_dir = '/usr/share/nginx/html/everCalck/templates_c/';
        $this->config_dir = '/usr/share/nginx/html/everCalck/configs/';
        $this->cache_dir = '/usr/share/nginx/html/everCalck/cache/';

        $this->caching = true;
        $this->assign('app_name', 'calculate everything');
    }
}