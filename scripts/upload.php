<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.11.16
 * Time: 22:26
 */


if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
{
    // Если файл загружен успешно, перемещаем его
    // из временной директории в конечную
    move_uploaded_file($_FILES["filename"]["tmp_name"], "/path/to/file/".$_FILES["filename"]["name"]);
} else {
    echo("Ошибка загрузки файла");
}
