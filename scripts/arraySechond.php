<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.11.16
 * Time: 23:29
 */

$deleteWords = '
абакан;
абакане;
абзаково;
абрау;
абхазии;
авиамоторная;
аврора;
автозавод;
автозаводе;
автозаводская;
автозаводский;
автозаводской;
автозаводском;
агатовые;
адлер;
адлере;
адыгея;
азербайджане;
азове;
академгородке;
академическая;
акбулаке;
актау;
алании;
алапаевске;
александрии;
александров;
александрове;
алексеевка;
аликанте;
алма;
алматы;
алтае;
алтайске;
алтайский;
алтуфьево;
алушта;
алуште;
альгешево;
альметьевске;
амстердаме;
амуре;
анапа;
анапе;
анапы;
ангарске;
анджелесе;
анжеро;
анталии;
антраците;
апрелевке;
арбате;
арбеково;
арзамасе;
армавир;
армавире;
артеме;
артемовске;
архангельск;
архангельске;
архызе;
астана;
астане;
астрахани;
астрахань;
атинская;
атырау;
ахтарске;
ачинск;
ашдоде;
аэрофлотский;
бабурка;
бабурке;
бабушкинская;
багратионовская;
бажана;
базар;
байкальске;
баймаке;
бакинской;
баксане;
баку;
балаклаве;
балаково;
балахне;
балашиха;
балашихе;
бали;
балта;
бангкоке;
банном;
барабинске;
барановичах;
барановичи;
барнаул;
барнауле;
барселоне;
баскунчак;
бат;
баторе;
батуми;
бахмуті;
бахчисарае;
бежице;
белгород;
белгороде;
белебее;
белово;
белогорске;
белокурихе;
белорецке;
белоруссии;
белорусская;
белорусском;
белостоке;
белые;
беляево;
бендерах;
бердске;
бердянск;
бердянска;
бердянске;
берегово;
березе;
березниках;
березовка;
березовский;
березовском;
берлине;
бескудниковский;
бийске;
бильярдной;
биробиджане;
бирюлево;
бич;
бишкек;
бишкека;
бишкеке;
благовещенск;
благовещенске;
блохина;
бобруйске;
болшево;
большевиков;
бонжур;
борисове;
борисоглебске;
борисполе;
боровичах;
боровичи;
брагино;
браилки;
братеево;
братиславская;
братиславской;
братске;
брачную;
брест;
бресте;
бриз;
бруклине;
брянск;
брянске;
бугуруслане;
будапеште;
буденновске;
бузулуке;
буковеле;
булгакова;
бургасе;
бурштині;
бутово;
буян;
вазове;
валенсии;
валуйках;
варне;
варшаве;
васильевском;
ваське;
вдадимире;
вднх;
ве;
вегас;
великий;
великих;
великом;
вене;
вернадского;
верхней;
верхняя;
ветеранов;
ветка;
вешенской;
видно;
видное;
видном;
вильнюсе;
винница;
виннице;
витебск;
витебске;
витязево;
вихідного;
вишневом;
влагостойкие;
владивосток;
владивостоке;
владикавказ;
владикавказе;
владимир;
владимире;
внуково;
во;
водный;
вознесенск;
войковский;
войковской;
волгоград;
волгограде;
волгодонск;
волгодонске;
волжск;
волжский;
волжском;
волновахе;
вологда;
вологде;
вольнянск;
воркуте;
воронеж;
воронеже;
воронежской;
воскресенске;
восточный;
воткинск;
воткинске;
всеволожске;
вске;
выборг;
выборге;
выксе;
вытегре;
выхино;
вьетнаме;
гагарина;
галицыно;
гатчина;
гатчине;
гелевыми;
геленджик;
геленджике;
генерала;
геническе;
германии;
героев;
герой;
герцена;
глазове;
гоа;
голубинский;
гольяново;
гомеле;
гомель;
гонконге;
горках;
горловке;
горно;
горном;
городке;
городок;
горячем;
гр;
грек;
греции;
грибоедова;
григоренко;
гродно;
грозном;
громкую;
гуанчжоу;
губкинском;
гусь;
д;
дагомысе;
дали;
дарница;
дарницкий;
девяткино;
дедовске;
денье;
дербышках;
десна;
деть;
джалиль;
джанкое;
джанкой;
джомтьене;
джубге;
дзержинск;
дзержинске;
дзержинский;
дивеево;
дики;
димитровграде;
димитрове;
динская;
дкп;
дмитрия;
дмитров;
дмитрове;
дмитровская;
дмитровский;
дне;
днепр;
днепра;
днепре;
днепродзержинск;
днепропетровск;
днепропетровска;
днепропетровске;
днепропетровской;
днепрорудном;
доброполье;
добруше;
добрый;
довгий;
долгодеревенское;
долгой;
долгопрудном;
долгопрудный;
домбае;
доминикане;
домна;
домодедово;
домодедовская;
донецк;
донецкая;
донецке;
донецкой;
донского;
дону;
дорогожичах;
доу;
дочки;
древлянка;
дружковке;
дубае;
дубровка;
душанбе;
дюрсо;
дюртюлях;
евпатории;
евро;
егорьевске;
ейск;
ейске;
екатерининском;
екб;
елабуге;
ельце;
ельцовка;
ереване;
ессентуках;
ессентуки;
жаворонках;
жвачки;
железноводск;
железноводске;
жигулевске;
житомир;
житомире;
жлобине;
жовтневый;
жодино;
жукова;
жуковский;
жуковском;
заводской;
заводском;
завьялово;
загара;
загорянке;
закарпатье;
залесский;
залесском;
запорожье;
запрудной;
заречье;
засвияжье;
засека;
звенигороде;
здається;
зеленка;
зеленограде;
зеленодольск;
зеленодольске;
златоусте;
зуево;
ивано;
иванове;
иваново;
ивантеевке;
ижевск;
ижевске;
измаил;
измаиле;
измайлово;
израиле;
израиль;
изюм;
илецке;
ильичевский;
ингульце;
индии;
йорке;
йошкар;
иркутск;
иркутске;
ирпене;
искитим;
испании;
истре;
италии;
ишиме;
кабардинка;
кабардинке;
кадастровый;
казани;
казань;
казахстан;
калининград;
калининграде;
калининградской;
калининец;
калининский;
калининском;
калинковичах;
калуга;
калуге;
калужская;
камелот;
каменец;
каменке;
каменномостском;
каменогорске;
каменск;
каменце;
камера;
камерой;
камчатского;
камчатском;
камышин;
камышине;
канарах;
канаш;
канаше;
каневской;
канске;
капотня;
кара;
караганда;
караганде;
каракозова;
карелии;
карла;
карташова;
каспийске;
каховке;
каховская;
качинцев;
качинцева;
качканаре;
кейт;
кемере;
кемерово;
керек;
керчи;
керчь;
киве;
киев;
киева;
киеве;
киевская;
киевский;
києві;
київі;
кимры;
кинешме;
кипре;
киришах;
киров;
кировграде;
кирове;
кировоград;
кировограде;
кировске;
кировский;
кировском;
кисловодск;
кисловодске;
кислород;
китае;
кишиневе;
климовск;
клин;
клину;
ко;
кобрине;
ковров;
коврове;
ковылкино;
когалым;
когалыме;
кожухово;
козельск;
козельске;
кокшетау;
коле;
коломенская;
коломна;
коломне;
коломыя;
колпино;
комар;
комара;
комсомольск;
комсомольске;
комсомольской;
конотоп;
конотопе;
константиновка;
константиновке;
копейск;
копейске;
коркино;
кормят;
королев;
королеве;
короткого;
коротчаево;
коса;
косе;
косино;
косиора;
костанае;
кострома;
костроме;
костюшко;
котельники;
котельниково;
котлас;
котласе;
котловка;
коты;
кохма;
кошелев;
крае;
край;
краков;
краматорск;
краматорске;
красноармейск;
красноармейске;
красноармейском;
красногвардейская;
красногорск;
красногорске;
красногорский;
краснодар;
краснодаре;
краснодарском;
красной;
красном;
красноперекопск;
красноперекопске;
красносельская;
краснотурьинск;
красноярск;
красноярске;
крейда;
кременчуг;
кременчуге;
кривогуза;
кривой;
кривом;
кронштадт;
кропоткин;
крылатый;
кстово;
кубани;
кубинка;
кугесях;
кузнецке;
кузьминки;
куйбышев;
куйбышеве;
кукушкой;
кумертау;
кунгуре;
купчино;
курахово;
курган;
кургана;
кургане;
куриленко;
курск;
курская;
курске;
курской;
куршская;
куршской;
куте;
куфар;
кучино;
кызыле;
лагонаки;
ладушки;
лазаревская;
лазаревском;
лазурного;
ланка;
лаппеенранте;
ларнаке;
лбз;
левый;
ленина;
ленинградская;
ленино;
лениной;
ленинск;
ленинске;
ленинский;
ленинском;
ленты;
лены;
лермонтова;
лермонтове;
лермонтовский;
лесной;
лесосибирск;
лесосибирске;
летию;
лимане;
лимассоле;
лимбяяха;
линника;
липецк;
липецке;
лисичанске;
лисках;
листвянка;
листвянке;
лихославль;
лобне;
лобня;
ломоносове;
лондоне;
лоо;
лос;
лосиноостровской;
лофт;
луга;
луганске;
лузановка;
лузановке;
луках;
лукьяновка;
луцке;
лучи;
лыскове;
лысьва;
лыткарино;
львов;
львове;
люберцах;
люберцы;
люблине;
люблино;
магадане;
магнитогорск;
магнитогорске;
мадриде;
майами;
майкоп;
майкопе;
майкопский;
макао;
макеевка;
макеевке;
малаховке;
мангуп;
мансийске;
марий;
мариинск;
марина;
мариуполе;
мариуполь;
маркса;
марокко;
маршала;
марьино;
масельского;
махачкала;
махачкале;
медведково;
медгородок;
международный;
междуреченск;
междуреченске;
мелеуз;
мелитополе;
мелитополь;
мелькомбината;
мехзаводе;
миассе;
милане;
минеральные;
минеральных;
минск;
минска;
минске;
минусинске;
миргород;
миргороде;
мирном;
мирный;
мисхор;
митино;
михайловке;
мичуринске;
могилеве;
модерна;
можайск;
моздоке;
мозыре;
молодежная;
молодежной;
молодежный;
морозовское;
мортон;
моршине;
москва;
москвы;
московке;
московская;
московский;
московской;
московском;
мостовской;
мостовском;
мукачево;
мулино;
мурманск;
мурманске;
мурманской;
муром;
муроме;
мценске;
мысхако;
мытищах;
мытищи;
мытищинский;
мюнхене;
набережной;
набережные;
набережных;
надым;
нальчик;
нальчике;
наро;
народження;
нахабино;
находка;
находке;
невинномысске;
невский;
невском;
нежине;
нефтекамске;
нефтеюганск;
нефтеюганске;
нивки;
нижегородская;
нижневартовск;
нижневартовске;
нижнедевицке;
нижнекамск;
нижнекамске;
николаев;
николаеве;
николаевка;
николаевке;
николаевская;
никополе;
никополь;
ницце;
новгород;
новгороде;
новогиреево;
новогоднюю;
новозаводская;
новозыбкове;
новокосино;
новокузнецк;
новокузнецке;
новокуйбышевск;
новокуйбышевске;
новомосковск;
новомосковске;
новопеределкино;
новопесчаная;
новополоцке;
новороссийск;
новороссийске;
новосибирск;
новосибирска;
новосибирске;
новососедово;
новотроицке;
новохоперске;
новочебоксарск;
новочебоксарске;
новочеркасске;
новошахтинск;
ногинск;
ногинске;
норильске;
нюрнберге;
нячанге;
оаэ;
обнинск;
обнинске;
оболонский;
оболонь;
одесса;
одессе;
одессы;
одинцова;
одинцово;
одинцовский;
одинцовского;
одинцовском;
озерки;
октябрьский;
октябрьском;
олимпийской;
омск;
омске;
оптиков;
оптина;
орел;
оренбург;
оренбурге;
орехово;
орлами;
орле;
орске;
оскол;
осколе;
отрадном;
отрожка;
павелецкая;
павелецкой;
павлове;
павловск;
павловске;
павлоград;
павлограде;
павлодар;
павлодаре;
павшино;
париж;
париже;
паттайе;
паттайя;
пекине;
пенза;
пензе;
первомайская;
первоуральск;
первоуральске;
перевальное;
переславле;
переславль;
перово;
петербург;
петербурга;
петербурге;
петергоф;
петергофе;
петровско;
петрозаводск;
петрозаводске;
петропавловск;
петропавловска;
петропавловске;
печатники;
пинске;
пионерская;
пиратское;
пироговской;
питер;
питера;
питере;
планерная;
пласту;
поварово;
поворова;
подгорице;
подлипках;
подмосковье;
подобово;
подольск;
подольске;
подольском;
подпорожье;
подселение;
пойма;
полежаевская;
полоцке;
полтава;
полтаве;
польском;
польше;
поляна;
поляне;
посад;
посаде;
посадки;
посадочные;
почаеве;
правды;
прага;
праге;
преображенской;
приднепровске;
прилуки;
приморск;
приморско;
приморском;
прокопьевске;
пролетарск;
пролетарская;
пролетарском;
проспект;
проспекте;
протвино;
профсоюзная;
профсоюзов;
псков;
пскове;
пушкин;
пушкина;
пушкине;
пушкино;
пушкинских;
пушкинской;
пушнине;
пхукет;
пхукете;
пыть;
пышме;
пятигорск;
пятигорске;
рабочий;
разумовская;
раменский;
раменское;
раменском;
рамешки;
ревда;
ревде;
реутов;
реутове;
реутово;
речной;
речном;
ржеве;
риге;
риме;
рио;
рог;
роге;
рогу;
родино;
родионова;
родник;
родники;
рокоссовского;
российская;
ростов;
ростова;
ростове;
ростовская;
роял;
рудном;
рудный;
руза;
русиянова;
рускеала;
руссе;
рф;
рыбинске;
рябково;
рязанский;
савеловская;
саках;
саки;
салават;
салавате;
сале;
салехард;
салтовка;
самара;
самаре;
самарская;
самарском;
самуи;
санкт;
сао;
сапсане;
саранск;
саранске;
сарапуле;
саратов;
саратове;
сатке;
саха;
сахалинск;
сахалинске;
свалява;
свао;
свердловский;
свердловской;
светлогорск;
светлогорске;
светлом;
светового;
святогорск;
святогорске;
святогорье;
святошино;
святошинский;
северном;
северодвинске;
северодонецке;
северской;
селенитовая;
сенная;
сергиев;
сергиевом;
серове;
серпухов;
серпухове;
сертолово;
сестрорецке;
сеуле;
сибае;
симферополя;
сингапуре;
сипайлово;
скобелевская;
скопин;
скретч;
славгород;
славкурорт;
славянск;
славянске;
славянский;
слуцке;
смоленск;
смоленске;
снежногорске;
советский;
советском;
совхозная;
сокол;
соколе;
солигорск;
солнечногорск;
солнцево;
соломенский;
сормово;
сормовском;
сорочинске;
сортавала;
соцгород;
сочи;
спортивная;
ставрополе;
ставрополь;
стамбуле;
стан;
стане;
станица;
старобельске;
старокачаловская;
старокостянтинові;
старолеушковской;
стаханове;
стерлитамак;
стерлитамаке;
строгино;
струсто;
стяжка;
судаке;
судженске;
суздале;
сургут;
сургуте;
сухум;
сухуми;
схи;
сходне;
сходненская;
сходненской;
сходня;
сша;
сызрани;
сызрань;
сыктывкар;
сыктывкаре;
таганрог;
таганроге;
таганской;
тагиле;
таиланд;
тайланде;
тайных;
таллине;
тамани;
тамань;
тамбов;
тамбове;
танковая;
таразе;
тарко;
тарту;
татарстан;
татищева;
ташкент;
ташкенте;
тбилиси;
твери;
тверь;
теберда;
теберде;
текстильщики;
тель;
темиртау;
темрюк;
тенерифе;
терновка;
тернополь;
терраса;
тимашевск;
тимирязевская;
тирасполе;
тихвине;
тихорецк;
тихорецке;
тобольск;
тобольске;
токио;
толстопальцево;
тольятти;
томск;
томске;
топлистан;
торжке;
торжок;
торонто;
торревьеха;
торревьехе;
тосно;
тостапалсево;
треск;
троещина;
троещине;
троицк;
трускавец;
трускавце;
трусово;
трусовском;
туапсе;
туймазах;
туймазы;
тула;
туле;
тулуне;
тульская;
тульской;
тупик;
тура;
туринск;
турции;
турция;
тутаеве;
тучково;
тушино;
тында;
тынде;
тюмени;
тюмень;
углич;
угличе;
удэ;
ужгород;
ужгороде;
узловой;
украина;
улан;
ульяновск;
ульяновска;
ульяновске;
умани;
умань;
уралмаше;
уральске;
уренгое;
уренгой;
усинск;
усинске;
уссурийске;
усть;
устюге;
ухте;
ухтомская;
учалах;
учкуевка;
фабричной;
феодосии;
феодосия;
фили;
филиппинах;
финляндии;
фоминске;
франковске;
фрунзе;
фрунзенская;
фрунзенской;
фрунзенском;
хабаровск;
хабаровске;
хабезе;
хадыженск;
хадыженске;
хайфе;
ханты;
харьков;
харькове;
харьковская;
хвалынск;
хельсинки;
херсон;
херсоне;
хижняка;
химках;
химки;
химмаше;
хлопки;
хмельник;
хмельнике;
хмельницкий;
хортицкий;
хотин;
хрустальном;
хуахине;
хургаде;
хуст;
царицыно;
царское;
цветение;
цветной;
цветочный;
ціни;
чайковском;
чанг;
чанге;
чебаркуль;
чебоксарах;
чебоксары;
челнах;
челны;
челябинск;
челябинске;
челябинский;
чемал;
череповец;
череповце;
черкасах;
черкассах;
черкассы;
черкесске;
черкизовской;
чернигов;
чернигова;
чернигове;
черниковка;
черниковке;
чернігів;
чернігові;
черновцы;
черногории;
черногорске;
черноморка;
черны;
черняховске;
чехии;
чехов;
чехове;
чита;
чите;
чупряково;
шаболовская;
шадринске;
шанхае;
шахтинский;
шахты;
шевченковский;
шелехов;
шерегеш;
шерегеше;
шигоны;
шикарная;
шорохи;
шостке;
шуе;
шуй;
шукаю;
шую;
шымкент;
шымкенте;
щегловская;
щекино;
щелково;
щелковская;
щелковский;
щелковское;
щелковской;
щелчки;
щербинка;
щербинке;
эйлате;
экибастузе;
эл;
элат;
электрогорск;
электростали;
электросталь;
электроугли;
элиста;
элисте;
элитная;
элитное;
элитные;
эльбрус;
энгельс;
энгельсе;
эссене;
юао;
ювао;
юго;
юдине;
южная;
южно;
южноукраинске;
южный;
юрга;
юрге;
юрьев;
юрюзани;
яготине;
яграх;
як;
якутск;
якутске;
ялта;
ялте;
яме;
янаул;
янтарная;
янтарной;
яр;
яре;
яремче;
яровом;
ярославле;
ярославль;
ярославский;
ясенево;
яскравий;
яуза;
яхе;
яхроме;
';